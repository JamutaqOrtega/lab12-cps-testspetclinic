package com.tecsup.petclinic.web;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.JsonPath;
import com.tecsup.petclinic.domain.Owner;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class OwnerControllerTest {

	private static final ObjectMapper om = new ObjectMapper();

	@Autowired
	private MockMvc mockMvc;

	
	@Test
	public void testGetOwners() throws Exception {
		int ID_FIRST_RECORD = 1;

		this.mockMvc.perform(get("/owners"))
				.andExpect(status().isOk()) //estado 200 esperado
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)) //formato JSON esperado
				.andExpect(jsonPath("$[0].id", is(ID_FIRST_RECORD))); //se espera que el 1er elemento sea de id 1
	}
	
	
	@Test
	public void testFindOwnerOK() throws Exception {
		String NOMBRE = "Jean";
		String APELLIDO = "Coleman";
		String ADDRESS = "105 N. Lake St.";
		String CIUDAD = "Monona";
		String TELEFONO = "6085552654";

		mockMvc.perform(get("/owners/6"))
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8)) //formato JSON esperado
				.andExpect(status().isOk()) //estado 200 esperado
				.andExpect(jsonPath("$.id", is(6))) //ID esperado
				.andExpect(jsonPath("$.firstName", is(NOMBRE))) //nombre esperado
				.andExpect(jsonPath("$.lastName", is(APELLIDO))) //apellido esperado
				.andExpect(jsonPath("$.address", is(ADDRESS))) //direccion esperada
				.andExpect(jsonPath("$.city", is(CIUDAD))) //ciudad esperada
				.andExpect(jsonPath("$.telephone", is(TELEFONO)));
	}
	
	
	@Test
	public void testFindOwnerKO() throws Exception {
		mockMvc.perform(get("/owners/666"))
				.andExpect(status().isNotFound()); //estado 404 esperado
	}
	
	
	@Test
	public void testCreateOwner() throws Exception {
		String NOMBRE = "Jamutaq";
		String APELLIDO = "Piero";
		String ADDRESS = "Av. ABC 123";
		String CIUDAD = "Lima";
		String TELEFONO = "123456789";

		Owner newOwner = new Owner(NOMBRE, APELLIDO, ADDRESS, CIUDAD, TELEFONO);

		mockMvc.perform(post("/owners")
				.content(om.writeValueAsString(newOwner)).header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print()) //mostrar el nuevo owner creado
				.andExpect(status().isCreated()) //estado 201 esperado
				.andExpect(jsonPath("$.firstName", is(NOMBRE)))
				.andExpect(jsonPath("$.lastName", is(APELLIDO)))
				.andExpect(jsonPath("$.address", is(ADDRESS)))
				.andExpect(jsonPath("$.city", is(CIUDAD)))
				.andExpect(jsonPath("$.telephone", is(TELEFONO)));
	}
	
	
	@Test
	public void testDeleteOwner() throws Exception {
		String NOMBRE = "Kiana";
		String APELLIDO = "Kaslana";
		String ADDRESS = "Av. XYZ 555";
		String CIUDAD = "ABC";
		String TELEFONO = "987654321";

		Owner newOwner = new Owner(NOMBRE, APELLIDO, ADDRESS, CIUDAD, TELEFONO);

		ResultActions mvcActions = mockMvc.perform(post("/owners")
				.content(om.writeValueAsString(newOwner))
				.header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isCreated()); //crea el objeto, se espera el estado 201

		String response = mvcActions.andReturn().getResponse().getContentAsString();

		Integer id = JsonPath.parse(response).read("$.id");

		mockMvc.perform(delete("/owners/" + id)) //elimina el objeto
				.andExpect(status().isOk()); //estado 404 esperado
	}


}
